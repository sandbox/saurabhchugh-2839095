                                                                                
General Information
-------------------
The tiny Scrollbar module integrates the tinyscrollbar jQuery plugin with 
Drupal. The tiny Scrollbar module allows the user to add contemporary minimalist
scrollbars to sections of content within a Drupal website.  There are options to
change the scrollbar settings and the animation of scroll-enabled-content as
well as a range of other features.

The aim of this module is to provide an easy implementation of many of
the standard features of this plugin for non-technical users of Drupal.

Compatibility Advice - jQuery Update Module
-------------------------------------------

There is NO dependency between this Module and the jQuery Update Module.

 'https://drupal.org/project/jquery_update'

For users of the jQuery Update Module version 7.x-2.3 (the current version)
The module is tested with jQuery 1.4.4 and will support up to version 10.0.

